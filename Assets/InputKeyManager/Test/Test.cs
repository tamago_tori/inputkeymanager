﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var inputManager = GetComponent<InputKeyManager>();
        var keyboard = GetComponent<InputKeyboard>();

        keyboard.AddKey(1, KeyCode.W);
        inputManager.AddEventListener(InputKeyManager.InputKeyStatus.Down, 1, (index) => {
            Debug.Log("down W");
        });
        inputManager.AddEventListener(InputKeyManager.InputKeyStatus.Up, 1, (index) => {
            Debug.Log("up W");
        });
        inputManager.AddEventListener(InputKeyManager.InputKeyStatus.Stay, 1, (index) => {
            Debug.Log("stay W");
        });
        inputManager.AddEventListener(InputKeyManager.InputKeyStatus.Out, 1, (index) => {
            Debug.Log("out W");
        });
    }
	

}
