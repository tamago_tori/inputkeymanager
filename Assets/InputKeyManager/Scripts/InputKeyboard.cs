﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(InputKeyManager))]
public class InputKeyboard : MonoBehaviour {

    InputKeyManager inputKeyManager;
    Dictionary<int, KeyCode> targetKeyDictionary = new Dictionary<int, KeyCode>();

    void Awake() {
        inputKeyManager = GetComponent<InputKeyManager>();
        if (inputKeyManager == null)
            throw new System.Exception("not found InputKeyManager component in the same object");
    }

	// Update is called once per frame
	void Update () {
	    foreach(var pair in targetKeyDictionary) {
            CheckAndRunKey(pair.Value, pair.Key);
        }
	}

    public void AddKey(int index, KeyCode code) {
        targetKeyDictionary[index] = code;
    }

    public void ExchangeKey(int index, int index2) {
        var temp = targetKeyDictionary[index];
        targetKeyDictionary[index] = targetKeyDictionary[index2];
        targetKeyDictionary[index2] = temp;
    }

    public void RemoveKey(int index) {
        targetKeyDictionary.Remove(index);
    }

    public void ClearKey() {
        targetKeyDictionary.Clear();
    }

    void CheckAndRunKey(KeyCode code, int index) {
        if (Input.GetKeyDown(code)) {
            inputKeyManager.Down(index);
        }
        if (Input.GetKeyUp(code)) {
            inputKeyManager.Up(index);
        }
    }
}
