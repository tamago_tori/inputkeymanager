﻿using UnityEngine;
using System.Collections.Generic;

public class InputKeyManager : MonoBehaviour {

    //delegate,event
    public delegate void InputKeyEventHandler(int index);

    //field
    Dictionary<int, List<InputKeyEventHandler>> KeyDownEventListener = new Dictionary<int, List<InputKeyEventHandler>>();
    Dictionary<int, List<InputKeyEventHandler>> KeyUpEventListener = new Dictionary<int, List<InputKeyEventHandler>>();
    Dictionary<int, List<InputKeyEventHandler>> KeyStayEventListener = new Dictionary<int, List<InputKeyEventHandler>>();
    Dictionary<int, List<InputKeyEventHandler>> KeyOutEventListener = new Dictionary<int, List<InputKeyEventHandler>>();
    Dictionary<int, InputKeyStatus> CurrentStatusDictionary = new Dictionary<int, InputKeyStatus>();
    Dictionary<InputKeyStatus, Dictionary<int, List<InputKeyEventHandler>>> KeyEventDictionary = new Dictionary<InputKeyStatus, Dictionary<int, List<InputKeyEventHandler>>>();
    bool isInit = false;

	// Use this for initialization
	void Awake () {
        isInit = true;
        KeyEventDictionary.Add(InputKeyStatus.Down, KeyDownEventListener);
        KeyEventDictionary.Add(InputKeyStatus.Up, KeyUpEventListener);
        KeyEventDictionary.Add(InputKeyStatus.Stay, KeyStayEventListener);
        KeyEventDictionary.Add(InputKeyStatus.Out, KeyOutEventListener); 
	}
    
    void Update() {
        foreach(var pair in CurrentStatusDictionary) {
            var index = pair.Key;
            var status = pair.Value;
            Run(index, status);
        }

        UpdateStatus();
    }

    //controller

    public void Down(int index) {
        CurrentStatusDictionary[index] = InputKeyStatus.Down;
    }

    public void Up(int index) {
        CurrentStatusDictionary[index] = InputKeyStatus.Up;
    }

    //event listener
    public void AddEventListener(InputKeyStatus status, int index, InputKeyEventHandler handler) {
        CheckInit();

        if(!KeyEventDictionary[status].ContainsKey(index)) {
            KeyEventDictionary[status][index] = new List<InputKeyEventHandler>();
        }
        CurrentStatusDictionary[index] = InputKeyStatus.Out;
        KeyEventDictionary[status][index].Add(handler);
    }

    public void RemoveEventListener(InputKeyStatus status, int index, InputKeyEventHandler handler) {
        CheckInit();

        KeyEventDictionary[status][index].Remove(handler);
    }

    public void ClearEventListener(InputKeyStatus status, int index, InputKeyEventHandler handler) {
        CheckInit();

        KeyEventDictionary[status][index].Clear();
    }

    public void ClearAll() {
        CheckInit();

        foreach(var eventDic in KeyEventDictionary) {
            foreach(var handlerDic in eventDic.Value) {
                handlerDic.Value.Clear();
            }
        }
    }

    public void ResetStatus() {
        CheckInit();

        foreach(var pair in CurrentStatusDictionary) {
            CurrentStatusDictionary[pair.Key] = InputKeyStatus.Out;
        }
    }

    //private

    void Run(int index, InputKeyStatus status) {
        if (!KeyEventDictionary[status].ContainsKey(index)) return;
        var handlerList = KeyEventDictionary[status][index];
        if (handlerList.Count == 0) return;
        foreach(var handler in handlerList) {
            handler(index);
        }
    }

    void UpdateStatus() {
        var keys = new List<int>(CurrentStatusDictionary.Keys);
        foreach (var key in keys) {
            var currentStatus = CurrentStatusDictionary[key];
            if(currentStatus == InputKeyStatus.Down) {
                currentStatus = InputKeyStatus.Stay;
            }
            if(currentStatus == InputKeyStatus.Up) {
                currentStatus = InputKeyStatus.Out;
            }
            CurrentStatusDictionary[key] = currentStatus;
        }
    }

    void CheckInit() {
        if (isInit)
            return;

        throw new System.Exception("is not init yet");
    }

    //enum
    public enum InputKeyStatus {
        Out,Down,Up,Stay
    }

}
